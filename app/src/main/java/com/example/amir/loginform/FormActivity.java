package com.example.amir.loginform;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class FormActivity extends AppCompatActivity implements View.OnClickListener {
    EditText username;
    EditText password;
    Button register;
    TextView result;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form);
        bindViews();
        register.setOnClickListener(this);
    }


    void onFormRegister()
    {
        String usernameval = username.getText().toString();
        String passval =password.getText().toString();
        result.setText(usernameval+" "+passval);
        username.setText("");
        password.setText("");
    }


    @Override
    public void onClick(View view)
    {
        if (view.getId()==R.id.register)
        {
            onFormRegister();
            Toast.makeText(this, "Registered", Toast.LENGTH_SHORT).show();
        }

    }


    void bindViews()
    {
        username = findViewById(R.id.user_name);
        password = findViewById(R.id.password);
        register = findViewById(R.id.register);
        result = findViewById(R.id.result);
    }


}